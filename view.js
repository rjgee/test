var View = function() {

    var ret = {
        init: function() {
            var amountInput = document.getElementById("amount");
            amountInput.onchange = onAmountChange;
            
            var expMonthInput = document.getElementById("expirationMonth");
            expMonthInput.onchange = onExpirationMonthChanged;
            
            var expYearInput = document.getElementById("expirationYear");
            expYearInput.onchange = onExpirationYearChanged;
            
            var ccNumberInput = document.getElementById("creditCardNumber");
            ccNumberInput.onchange = onCreditCardNumberChanged;
            
            var submitButton = document.getElementById("submit");
            submitButton.onclick = onSubmitClicked;
        },                    
    };
    return ret;
}