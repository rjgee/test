var Controller = function(m) {
    var myModel = m;
    
    var onAmountChange = function() {
        myModel.setAmount(this.value);
        console.log("amount changed to " + myModel.getAmount());
    };
    
    var onCreditCardNumberChanged = function() {
        myModel.setCreditCardNumber(this.value);
        console.log("credit card number changed to " + myModel.getCreditCardNumber());
    };
    
    var onExpirationMonthChanged = function() {
        myModel.setExpirationMonth(this.value);
        console.log("expiration month changed to " + myModel.getExpirationMonth());
    };
    
    var onExpirationYearChanged = function() {
         myModel.setExpirationYear(this.value);
         console.log("expiration year changed to " + myModel.getExpirationYear());
    };
    
    var onSubmitClicked = function() {
        var status;
        var onDone = function() {
            status = "load successfully";
            console.log("the submit status is " + status);
        }
        var onError = function() {
            status = "load error";
            console.log("the submit status is " + status);
        }
        myModel.submit(onDone, onError);

    };
    

}