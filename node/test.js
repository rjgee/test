var assert = require('chai').assert;
var findDuplicates = require('./index.js').findDuplicates;

describe('findDuplicates function', function() {
    it('finds duplicate elements in an array', function() {
    var values = [1,2,2,2,2,2,556,77,8,9,33,2,4,5,7,1,1,1,6,77];
    var result = findDuplicates(values);
    
    assert.include(result, 1, 'did not find 1');
    assert.include(result, 2, 'did not find 2');
    assert.include(result, 77, 'did not find 77');
    });
});
