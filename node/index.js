/*
function functionScopedClosureExample() {
    var ret = [];
    for(var i = 0; i < 5; i++) {
        var j = i;
        ret.push(    
            (function(x){
                return function() {
                    console.log(x);
                }
            })(j)
        );
    }
    
    return ret;
}

var stuffToPrint = functionScopedClosureExample();

stuffToPrint.forEach(function(x) {
    x();
});
*/

module.exports.findDuplicates = function(arr) {
    var dupes ={};
    for(var i = 0, len = arr.length; i < len; i++) {
        var element = arr[i];
        if(dupes[element.toString()] === undefined) {
            dupes[element.toString()] = 1;
        } else {
            dupes[element.toString()] = dupes[element.toString()] + 1;
        }
    }
    
    return arr.filter(function(value) {
        return dupes[value.toString()] > 1;
    });
}




