 var Model = function() {
                var innerModel = {
                    amount: 0,
                    creditCardNumber: "",
                    expirationMonth: "",
                    expirationYear: ""    
                };
                
                var ccModel = {
                    setAmount: function(value) {
                        // do validations or throw error
                        innerModel.amount = value;
                    },
                    getAmount: function() {
                        return innerModel.amount;
                    },
                    setCreditCardNumber: function(value) {
                        // do validations or throw error
                        innerModel.creditCardNumber = value;
                    },
                    getCreditCardNumber: function() {
                        return innerModel.creditCardNumber;
                    },
                    setExpirationMonth: function(value) {
                        // do validations or throw error
                        innerModel.expirationMonth = value;
                    },
                    getExpirationMonth: function() {
                        return innerModel.expirationMonth;
                    },
                    setExpirationYear: function(value) {
                        // do validations or throw error
                        innerModel.expirationYear = value;
                    },
                    getExpirationYear: function() {
                        return innerModel.expirationYear;
                    },

                    submit: function(done, error) {
                        
                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                        xmlhttp.onload = function() {
                            // in case of network errors this might not give reliable results
                            console.log("success");
                            done();
                        };
                        xmlhttp.onError = function() {
                            // in case of network errors this might not give reliable results
                            console.log("error");
                            error();
                        };
                        xmlhttp.open("POST", "http://mockbin.org/bin/6cd8f83d-be02-4885-88b8-0a11b5838511");
                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        xmlhttp.send(JSON.stringify(innerModel));
          
                    }

                       
                };
                return ccModel;
                
            };